#!/bin/bash

# INFO: Installs alberta3 (for dune-only), dune and amdis2 modules based on a toolchain and dune release
# AUTHOR: M. Mokbel (with help by L. Wittwer and Simon Praeterius)

### installing necessary software

sudo apt-get install \
libalberta-dev \
libmetis-dev \
libopenmpi-dev \
libparmetis-dev \
libsuitesparse-dev



DUNE_RELEASE="2.9"

## PUT YOUR DIRECTORY HERE ##

DUNE_CORE_MODULES="dune-common dune-geometry dune-grid dune-istl dune-localfunctions"
DUNE_STAGING_MODULES="dune-functions dune-typetree dune-uggrid"
DUNE_EXTENSIONS_MODULES="dune-alugrid dune-foamgrid dune-vtk dune-grid-glue"
DUNE_BACKENDS="ISTL"

#########################################################################
# YOU SHOULD NOT HAVE TO CHANGE ANYTHING BELOW
#########################################################################


# Paths, variables etc.


#GMSH_INSTALL_DIR="gmsh"
#ALBERTA_INSTALL_DIR="alberta3"
DUNE_INSTALL_DIR="dune"
AMDIS_INSTALL_DIR="dune"

#mkdir -p ${GMSH_INSTALL_DIR}
#mkdir -p ${ALBERTA_INSTALL_DIR}
mkdir -p ${DUNE_INSTALL_DIR}


#####################################################
# Installing gmsh
#####################################################
#GMSH_GIT="https://gitlab.onelab.info/gmsh/gmsh.git"
#GMSH_BUILD_DIR=${GMSH_INSTALL_DIR}/gmsh/build

#cd ${GMSH_INSTALL_DIR}
#git clone ${GMSH_GIT}
#cd gmsh

#mkdir build
#cd build
#cmake -DENABLE_BUILD_DYNAMIC=1 -DCMAKE_CXX_COMPILER=/software/haswell/Clang/9.0.1-GCCcore-8.3.0/bin/clang++ -DCMAKE_C_COMPILER=/software/haswell/Clang/9.0.1-GCCcore-8.3.0/bin/clang -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS=\"-march=haswell\" -DCMAKE_INSTALL_PREFIX=../include/ -DCMAKE_PREFIX_PATH=../include/ ..
#make -j4
#make install

#cd ../../

#####################################################
# Installing Alberta3, release/3.0.3 (for dune-only)
#####################################################
# NOTE: Could be optimised to have only one installation for each toolchain (and not dune release)

#ALBERTA_GIT="https://gitlab.com/alberta-fem/alberta3.git"
#ALBERTA_BUILD_DIR=${ALBERTA_INSTALL_DIR}/build

#cd ${ALBERTA_INSTALL_DIR}

# clonig git repo and checkout release 3.0.3
#git clone ${ALBERTA_GIT} .

#git checkout releases/3.0.3

#mkdir build

# config and install
#./generate-alberta-automakefiles.sh
#autoreconf --force --install

#./configure --prefix=${ALBERTA_BUILD_DIR} --disable-fem-toolbox

#make -j4
#make install

#cd ..

# PKG_CONFIG_PATH is not correctly detected for the dune installation, lets set it
#export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${ALBERTA_BUILD_DIR}/lib/pkgconfig"


#############################################
# Create opts-Files
#############################################
mkdir ${DUNE_INSTALL_DIR}/opts
OPTS_FILES_DIR=${DUNE_INSTALL_DIR}/opts
OPTS_FILES=""

## change compiler options to match your compiler (find your compiler with "whereis clang") ##
for be in $DUNE_BACKENDS; do
	RELEASE_OPTS_TEXT=$(cat <<-EOM
    CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=RelWithDebInfo
                 -DCMAKE_CXX_COMPILER=/usr/bin/clang++-14
                 -DCMAKE_C_COMPILER=/usr/bin/clang-14
                 -DCMAKE_CXX_FLAGS=-w
                 -DDUNE_ENABLE_PYTHONBINDINGS:BOOL=0
                 -DDUNE_ENABLE_PYTHONBINDINGS=OFF"
    MAKE_FLAGS="-j2"
    BUILDDIR="build-clang"
EOM
)
	OPTS_FILE_NAME=clang.opts
	echo "${RELEASE_OPTS_TEXT}" > ${OPTS_FILES_DIR}/${OPTS_FILE_NAME}
	OPTS_FILES="${OPTS_FILES} ${OPTS_FILE_NAME}"
done


#############################################
# Installing DUNE and AMDiS2
#############################################

# downloading dune modules
cd ${DUNE_INSTALL_DIR}

for module in $DUNE_CORE_MODULES; do
	git clone https://gitlab.dune-project.org/core/${module}.git
done

for module in $DUNE_STAGING_MODULES; do
	git clone https://gitlab.dune-project.org/staging/${module}.git
done

for module in $DUNE_EXTENSIONS_MODULES; do
	git clone https://gitlab.dune-project.org/extensions/${module}.git
done

git clone https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgrid.git
git clone https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgeometry.git

PATH=dune-common/bin:$PATH
export DUNE_ROOT=.

# downloading amdis + amdis-extensions
cd ../${AMDIS_INSTALL_DIR}

git clone https://gitlab.com/amdis/amdis.git
git clone https://gitlab.mn.tu-dresden.de/amdis/amdis-extensions.git
#git clone https://gitlab.com/aland-lab/amdis-surfacebasis.git

# set the DUNE_CONTROL_PATH to include the amdis dir, exporting it s.t. dunecontrol finds it
export DUNE_CONTROL_PATH="./amdis:."


# checking out the correct release
#dune-common/bin/dunecontrol exec git checkout releases/2.9
#cd ${AMDIS_INSTALL_DIR}/amdis-surfacebasis
#git checkout main
#cd ${AMDIS_INSTALL_DIR}


# building the modules for all the opts files
for opt in $OPTS_FILES; do
	dune-common/bin/dunecontrol --opts=../${OPTS_FILES_DIR}/${opt} all
done

cd ..
git clone https://gitlab.com/aland-lab/marcelmokbel/breastcancer.git
cd breastcancer
export DUNE_CONTROL_PATH="../${AMDIS_INSTALL_DIR}/amdis:../${DUNE_INSTALL_DIR}/"
for opt in $OPTS_FILES; do
	../${DUNE_INSTALL_DIR}/dune-common/bin/dunecontrol --opts=../${OPTS_FILES_DIR}/${opt} --current all
done


echo "Installation done!"