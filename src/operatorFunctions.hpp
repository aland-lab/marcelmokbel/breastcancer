#ifndef BREASTCANCER_OPERATORFUNCTIONS_HPP
#define BREASTCANCER_OPERATORFUNCTIONS_HPP

///  \brief Fill all Cahn Hilliard operators into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param _phi: The treePath for the phase field
 *  \param _mu:  The treePath for the chemical potential
 *  \param _vGrid: The treePath for the velocity field
 *  \param phi: A discrete function representing the current phi
 *  \param randomDOF: A discrete function representing random numbers to multiply the source term with
 *  \param invTau:  The inverse of the time step size
 *  \param source:  The constant for the source term
 *  \param num: An indicator for the respective Cahn Hilliard equation. num==1 refers to the first CH eq.
 **/
template <class PS, class TP, class TP2,  class DF>
void fillOperatorsCH(PS& prob,
                     TP _phi,
                     TP2 _mu,
                     DF const& randomDOF,
                     std::reference_wrapper<double>& invTau,
                     std::reference_wrapper<double>& source,
                     int num = 1) {
    auto dow = Grid::dimensionworld;
    double M = Parameters::get<double>("parameters->M").value_or(1.0);
    double sigma = Parameters::get<double>("parameters->sigma").value_or(1.0);
    double eps = Parameters::get<double>("parameters->epsilon").value_or(0.02);

    auto phi = prob.solution(_phi);

    prob.addMatrixOperator(zot(invTau), _phi, _phi);
    prob.addVectorOperator(zot(invTau * phi), _phi);

    prob.addMatrixOperator(sot(M), _phi, _mu);
    prob.addMatrixOperator(zot(1.0), _mu, _mu);

    prob.addMatrixOperator(sot(-sigma * eps), _mu, _phi);

    auto opFimpl = zot(-sigma * 0.25 / eps * (2 + 12 * phi * (phi - 1)));
    prob.addMatrixOperator(opFimpl, _mu, _phi);

    auto opFexpl = zot(sigma * 0.25 / eps * pow <2>(phi) * (6 - 8 * phi));
    prob.addVectorOperator(opFexpl, _mu);

    auto opGrowth = makeOperator(tag::test_divtrialvec{},phi);
    prob.addMatrixOperator(opGrowth, _phi, _vGrid);

    // subtract tangential force from convective term
    double tangentialForce  = Parameters::get<double>("parameters->c").value_or(10);
    auto opTang2 = makeOperator(tag::gradtest_gradtrial{}, tangentialForce * phi, 4);
    prob.addMatrixOperator(opTang2, _phi, _lambda1);

    if (num == 1 || num == 3) {
        prob.addVectorOperator(zot(source * randomDOF * phi), _phi);
    } else if (num == 2) {
        auto t = Parameters::get<double>("threshold").value_or(0.9);
        prob.addVectorOperator(zot(source * phi * invokeAtQP([t](double phi) {return (phi < t) ? 1 : 0;}, clamp(phi,0.0,1.0)),4), _phi);
    }
}

///  \brief Fill all shell force operators due to Cahn-Hilliard contributions into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param _phi: The treePath for the phase field
 *  \param _vGrid: The treePath for the velocity field
 *  \param phi: A discrete function representing the current phi
 *  \param mu: A discrete function representing the current phi
 *  \param kappaVec: A discrete function representing the curvature vector
 **/
template <class PS, class TP, class TP2>
void fillOperatorsCHForce(PS& prob,
                          TP _phi,
                          TP2 _mu) {
    auto dow = Grid::dimensionworld;
    double preFactor = 1.0;

    auto phi = prob.solution(_phi);
    auto mu = prob.solution(_mu);
    auto kappaVec = prob.solution(_kappaVec);

    Parameters::get("parameters->k", preFactor);
    double eps = Parameters::get<double>("parameters->epsilon").value_or(0.02);
    auto opTens1 = makeOperator(tag::testvec_trial{},
                                -preFactor * kappaVec * (- mu
                                                         + 1.0/eps * 0.25 * phi*pow<2>(1.0-phi)
                                                         - 1.0/eps * 0.0 * (2.0*pow<2>(1-phi) - 2.0*phi*(1.0-phi))),4);
    prob.addMatrixOperator(opTens1, _vGrid, _phi);

    for (size_t i = 0; i < dow; ++i) {
        for (size_t j = 0; j < dow; ++j) {
            auto opTens1G = makeOperator(tag::test_partialtrial{j},
                                         -preFactor * get(kappaVec,i) * (- eps * 0.5 * get(gradientOf(phi),j)),4);
            prob.addMatrixOperator(opTens1G, makeTreePath(_vGrid,i), _phi);
        }
    }

}

///  \brief Fill all operators for the computation of the curvature vector into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param _kappaVec:  The treePath for the curvature vector
 *  \param _vGrid: The treePath for the velocity field
 *  \param tau:  The time step size
 **/
template <class PS>
void fillOperatorsCurvature(PS& prob,
                            std::reference_wrapper<double>& tau) {
    auto dow = Grid::dimensionworld;

    // kappaVec =
    auto opL = makeOperator(tag::testvec_trialvec{},1.0);
    prob.addMatrixOperator(opL,_kappaVec,_kappaVec);

    // <grad xS, grad v>
    for (int i = 0; i < dow; i++) {
        auto opLB = makeOperator(tag::gradtest_gradtrial{}, tau);
        prob.addMatrixOperator(opLB, makeTreePath(_kappaVec, i), makeTreePath(_vGrid, i));

        auto opLBRHS = makeOperator(tag::gradtest{}, -gradientOf(X(i)),2);
        prob.addVectorOperator(opLBRHS, makeTreePath(_kappaVec, i));
    }
}

///  \brief Fill all bending force operators into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param _kappaVec:  The treePath for the curvature vector
 *  \param kappaVec: A discrete function representing the current curvature vector
 **/
template <class PS, class DF>
void fillOperatorsBending(PS& prob,
                          DF const& kappaVec) {
    auto dow = Grid::dimensionworld;
    auto B = Parameters::get<double>("parameters->Kb").value_or(0.0);

    // <|kappaVec|^2, div v>
    auto nonlin1 = makeOperator(tag::divtestvec_trial{},
                                -0.5 * B * valueOf(kappaVec,0),4);
    prob.addMatrixOperator(nonlin1, _vGrid, makeTreePath(_kappaVec,0));

    auto nonlin1b = makeOperator(tag::divtestvec_trial{},
                                 -0.5 * B * valueOf(kappaVec,1),4);
    prob.addMatrixOperator(nonlin1b, _vGrid, makeTreePath(_kappaVec,1));


    // <grad kappaVec, grad v>
    for (std::size_t i = 0; i < dow; ++i) {
        auto laplaceKappa = makeOperator(tag::gradtest_gradtrial{}, -B,4);
        prob.addMatrixOperator(laplaceKappa, makeTreePath(_vGrid,i), makeTreePath(_kappaVec,i));
    }

    if (dow == 3) {
        // < div kappaVec, div v>
        auto divKappa = makeOperator(tag::divtestvec_divtrialvec{}, -B, 4);
        prob.addMatrixOperator(divKappa, _vGrid, _kappaVec);

        // <grad kappaVec, P((grad v)^T + grad v) P>
        // auto PGradP = makeOperator(tag::rateOfDeformation{}, B, 4); // rateOfDeformation{} needs to be implemented if world dimension is 3
        // prob.addMatrixOperator(PGradP, _vGrid, _kappaVec);
    } else {
        // < div kappaVec, div v>
        auto divKappa = makeOperator(tag::divtestvec_divtrialvec{}, B, 4);
        prob.addMatrixOperator(divKappa, _vGrid, _kappaVec);
    }
}

///  \brief Fill all grid velocity operators into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param _vGrid:  The treePath for the grid velocity vector
 *  \param _lambda1:  The treePath for the element stretch
 *  \param distances:  An ElementVector, containing in each element the stretch
 *                     compared to the initial state
 *  \param tau:  The time step size
 **/
template <class PS, class EV>
void fillOperatorsVGrid(PS& prob,
                        EV const& distances,
                        std::reference_wrapper<double>& tau) {
    // vGrid =
    auto opVGrid = makeOperator(tag::testvec_trialvec{},1.0);
    prob.addMatrixOperator(opVGrid, _vGrid, _vGrid);

    double tangentialForce  = Parameters::get<double>("parameters->c").value_or(10);
    auto opTang = makeOperator(tag::testvec_gradtrial{},-tangentialForce,4);
    prob.addMatrixOperator(opTang, _vGrid, _lambda1);

    // lambda1 =
    auto opX = makeOperator(tag::test_trial{}, 1.0, 2);
    prob.addMatrixOperator(opX, _lambda1,_lambda1);

    // = lambda1Old
    auto opL = makeOperator(tag::test{}, myValueOf(distances), 2);
    prob.addVectorOperator(opL, _lambda1);

    // -tau <lambda1 div v , psi>
    auto opL1DivV = makeOperator(tag::test_divtrialvec{},
                                 -(myValueOf(distances)) * tau, 2);
    prob.addMatrixOperator(opL1DivV, _lambda1, _vGrid);
}

///  \brief Fill all concentration diffusion equation operators into the problem
/**
 *  callable for multiple Cahn Hilliard equations
 *
 *  \param prob: The underlying ProblemStat
 *  \param invTau:  The inverse time step size
 **/
template <class PS>
void fillOperatorsConc(PS& prob,
                       std::reference_wrapper<double>& invTau) {
    auto dow = Grid::dimensionworld;
    auto D = Parameters::get<double>("parameters->diffusion").value_or(0.0);

    // <grad kappaVec, grad v>
    auto laplaceKappa = makeOperator(tag::gradtest_gradtrial{}, D,4);
    prob.addMatrixOperator(laplaceKappa, _conc, _conc);


    // add your operators here ...
    // (you may need to add more arguments to the function, depending on the equation you want to solve))

}

#endif //BREASTCANCER_OPERATORFUNCTIONS_HPP
