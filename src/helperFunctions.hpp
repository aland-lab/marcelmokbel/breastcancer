#pragma once

#include <amdis/gridfunctions/ElementGridFunction.hpp>
#include <amdis/ElementVector.hpp>
#include <dune/grid/albertagrid.hh>
#include "amdis/breastCancer/myIndices.hpp"
#include <amdis/breastCancer/writeFiles.hh>
#include <amdis/interpolators/AverageInterpolator.hpp>

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;
using namespace MyIndices;
using Grid = Dune::AlbertaGrid<1, 2>;


bool comparePoints(const Dune::FieldVector<double,Grid::dimensionworld>& p1, const Dune::FieldVector<double,Grid::dimensionworld>& p2) {
    double angle1 = atan2(p1[1], p1[0]);
    double angle2 = atan2(p2[1], p2[0]);
    return angle1 < angle2;
}

// sort surface grid indices counterclockwise by using the undeformed grid (which is convex)
void sortPointsByClockwise(std::vector<std::pair<FieldVector<double,Grid::dimensionworld>,FieldVector<double,Grid::dimensionworld>>>& points) {
    std::sort(points.begin(),points.end(), [&](const std::pair<FieldVector<double,Grid::dimensionworld>,FieldVector<double,Grid::dimensionworld>>& p1,
                                               const std::pair<FieldVector<double,Grid::dimensionworld>,FieldVector<double,Grid::dimensionworld>>& p2) {
        return comparePoints(p1.first, p2.first);
    });
}

// compute enclosed area of the surface mesh
template <class P>
double computeEnclosedArea(P const& prob) {
    auto const dow = Grid::dimensionworld;
    double area = 0;
    // first sort indices and coordinates of points counterclockwise
    auto const& gv = prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView();
    std::vector<std::pair<FieldVector<double,dow>,FieldVector<double,dow>>> indicesAndCoordsMap(gv.size(1));
    for (auto const& v : vertices(gv)) {
        auto x = v.geometry().corner(0);
        auto ind = gv.indexSet().index(v);
        indicesAndCoordsMap[ind] = std::make_pair(x,x);
    }

    for (auto const& e : elements(prob.gridView())) {
        auto v = e.template subEntity<1>(0);
        auto v2 = e.template subEntity<1>(1);
        auto x = e.geometry().corner(0);
        auto x2 = e.geometry().corner(1);
        auto ind = prob.gridView().indexSet().index(v);
        auto ind2 = prob.gridView().indexSet().index(v2);

        indicesAndCoordsMap[ind].second = x;
        indicesAndCoordsMap[ind2].second = x2;
    }
    sortPointsByClockwise(indicesAndCoordsMap);

    // use Gaussian trapezoid formula
    for (int i = 0; i < indicesAndCoordsMap.size(); ++i) {
        auto xi = indicesAndCoordsMap[i].second;
        auto xip1 = xi;
        if (i == indicesAndCoordsMap.size()-1) {
            xip1 = indicesAndCoordsMap[0].second;
        } else {
            xip1 = indicesAndCoordsMap[i+1].second;
        }
        area += 0.5 * (xi[1] + xip1[1]) * (xi[0] - xip1[0]);
    }
    return area;
}


/// compute current kappaVec with finite elements
template <class DV, class SG>
void computeKappaVecOld(DV& kappaVecOld,
                        SG surfaceGrid) {

    auto preBasis = composite(power<Grid::dimensionworld>(lagrange<1>(),flatInterleaved()), //coords
                              power<Grid::dimensionworld>(lagrange<1>(),flatInterleaved()),flatLexicographic());//kappaVecOld

    ProblemStat prob("kappaVecOld", *surfaceGrid, preBasis);
    prob.initialize(INIT_ALL);

    // x =
    auto opX = makeOperator(tag::testvec_trialvec{},1.0);
    prob.addMatrixOperator(opX,_0,_0);

    // = oldCoords
    auto opV = makeOperator(tag::testvec{},X(),4);
    prob.addVectorOperator(opV,_0);

    // L =
    auto opL = makeOperator(tag::testvec_trialvec{},1.0);
    prob.addMatrixOperator(opL,_1,_1);

    for (int i = 0; i < surfaceGrid->dimensionworld; i++) {
        auto opLB = makeOperator(tag::gradtest_gradtrial{}, 1.0);
        prob.addMatrixOperator(opLB, makeTreePath(_1, i), makeTreePath(_0, i));
    }

    AdaptInfo adaptInfo("adapt");
    prob.assemble(adaptInfo);
    prob.solve(adaptInfo);

    valueOf(kappaVecOld) << prob.solution(_1);
    prob.writeFiles(adaptInfo);
}

template <class PS, class DF>
void computeInitialPhaseField(PS& prob,
                              DF& phi,
                              AdaptInfo& adaptInfo,
                              int number) {
    auto const dow = Grid::dimensionworld;
    FieldVector<double, dow> center{0.5, 0.5};
    double radius1, radius2;
    Parameters::get("parameters->center", center);
    radius1 = Parameters::get<double>("parameters->radius1"
                                      + (number != 1 ? "_" + std::to_string(number) : "")).value_or(0.15);
    radius2 = Parameters::get<double>("parameters->radius2"
                                      + (number != 1 ? "_" + std::to_string(number) : "")).value_or(0.25);

    auto eps = Parameters::get<double>("parameters->epsilon").value_or(0.1);
    auto phase = [eps](auto s)
    {
        return 0.5*(1.0 - invokeAtQP(Operation::Tanh{}, evalAtQP(s)*(1.0/(4.0*std::sqrt(2.0)*eps))));
    };

    // if random values are desired, set initial to 1 in parameter file
    int initial = Parameters::get<int>("circles or initial value"
                                        + (number != 1 ? " " + std::to_string(number) : "")).value_or(0);

    if (initial) {
        double radius, radiusIn, initialVal, randomVal;
        radius = Parameters::get<double>("initial radius"
                                         + (number != 1 ? " " + std::to_string(number) : "")).value_or(0.15);
        radiusIn = Parameters::get<double>("initial radius inside"
                                           + (number != 1 ? " " + std::to_string(number) : "")).value_or(0.15);
        initialVal = Parameters::get<double>("phase field initial value"
                                             + (number != 1 ? " " + std::to_string(number) : "")).value_or(0);
        randomVal = Parameters::get<double>("phase field initial value random"
                                            + (number != 1 ? " " + std::to_string(number) : "")).value_or(0);

        auto randomDOF = [initialVal, randomVal, radius, radiusIn](FieldVector<double, Grid::dimensionworld> const &x) {
            return (std::pow(x[0], 2) + std::pow(x[1], 2) + (dow == 3) * std::pow(x[2], 2) <= std::pow(radius, 2) &&
                    std::pow(x[0], 2) + std::pow(x[1], 2) + (dow == 3) * std::pow(x[2], 2) >= std::pow(radiusIn, 2)) ?
                   initialVal + randomVal * ((double) rand() / (RAND_MAX)) : 0.0;
        };
        phi << randomDOF;
    }

    // set phase field based on a circle if no random values are computed
    // compute initial refinement of the grid
    int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
    for (int i = 0; i < ref_int; ++i) {
        if (!initial) {
            phi << 0.0;
            auto circleN = [radius1, radius2, &center](FieldVector<double, Grid::dimensionworld> const &x) {
                return (radius1 + radius2) * (std::sqrt(
                        Math::sqr((x[0] - center[0]) / radius1) + Math::sqr((x[1] - center[1]) / radius2)
                         + (dow == 3) * Math::sqr((x[2] - center[2]) / radius2)) -
                                              1.0);
            };
            phi += clamp(phase(circleN), 0.0, 1.0); // phase field only inside the shell
        }
        prob.markElements(adaptInfo);
        prob.adaptGrid(adaptInfo);
    }
}


///  \brief Write out different data for post processing
/**
 *  \param prob: The underlying ProblemStat
 *  \param adaptInfo: The time information of the simulation
 *  \param _kappaVec:  The treePath for the curvature vector
 *  \param _phi(2,3):  The treePath for the phaseField(s)
 *  \param E_sum:  The system's total energy
 **/
template <class P>
void writePostProcessingData(P const& prob,
                             AdaptInfo const& adaptInfo,
                             double& E_sum) {
    double preFactor = 1.0;
    Parameters::get("parameters->k", preFactor);
    double eps = Parameters::get<double>("parameters->epsilon").value_or(0.02);
    auto B = Parameters::get<double>("parameters->Kb").value_or(0);
    double sigma = Parameters::get<double>("parameters->sigma").value_or(1.0);
    auto kappaVec = prob.solution(_kappaVec);
    auto phi = prob.solution(_phi);
    auto phi2 = prob.solution(_phi2);
    auto phi3 = prob.solution(_phi3);

    double interfaceLength = integrate(1.0, prob.gridView(),2);
    double enclosedArea = computeEnclosedArea(prob);
    double massOfPhi = integrate(phi, prob.gridView(),2);
    double massOfPhi2 = integrate(phi2, prob.gridView(),2);
    double massOfPhi3 = integrate(phi3, prob.gridView(),2);

    // compute free energies
    double E_bend = integrate(0.5*B*unary_dot(kappaVec),prob.gridView(),4); // integrateShell(0.5 * B * unary_dot(valueOf(kappa)),prob.gridView(),partitions_,-1);
    double E_ch = integrate(sigma*(eps*0.5*unary_dot(gradientOf(phi)) + 0.25/eps*pow<2>(phi)*pow<2>(1.0-phi))
            ,prob.gridView(),4);
    double E_ch2 = integrate(sigma*(eps*0.5*unary_dot(gradientOf(phi2)) + 0.25/eps*pow<2>(phi2)*pow<2>(1.0-phi2))
            ,prob.gridView(),4);
    double E_ch3 = integrate(sigma*(eps*0.5*unary_dot(gradientOf(phi3)) + 0.25/eps*pow<2>(phi3)*pow<2>(1.0-phi3))
            ,prob.gridView(),4);

    double E_sum_old = E_sum;
    E_sum = E_bend + E_ch + E_ch2 + E_ch3 ;
    double dtEsum = (E_sum - E_sum_old) / adaptInfo.timestep();

    std::string path;
    Parameters::get("output directory", path);
    std::string output_filename;
    Parameters::get("post processing output filename", output_filename);
    output_filename = path + "/" + output_filename;
    FILE *file;
    if (adaptInfo.time() == adaptInfo.timestep()) {
        file = fopen(output_filename.c_str(), "w");
        fprintf(file, "time, interfaceLength, enclosedArea, massOfPhi, massOfPhi2, massOfPhi3, E_bend, E_ch, E_ch2, E_ch3, E_sum, dtE_sum\n");
        fclose(file);

    } else {
        file = fopen(output_filename.c_str(), "a");
        fprintf(file, "%10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f, %10.6f\n",
                adaptInfo.time(),interfaceLength,enclosedArea,massOfPhi,massOfPhi2,massOfPhi3,E_bend,E_ch,E_ch2,E_ch3,E_sum,dtEsum);
        fclose(file);
    }
}

//  \brief Write out different data for post processing
/**
 *  \param prob: The underlying ProblemStat
 *  \param adaptInfo: The time information of the simulation
 *  \param _kappaVec:  The treePath for the curvature vector
 *  \param _phi(2,3):  The treePath for the phaseField(s)
 **/
template <class W, class P, class DV>
void writeForceContributions(W& writer,
                             P const& prob,
                             DV const& kappaVecOld,
                             std::vector<double>& timesteps,
                             AdaptInfo const& adaptInfo) {
    auto const dow = Grid::dimensionworld;
    double preFactor = 1.0;
    Parameters::get("parameters->k", preFactor);
    double eps = Parameters::get<double>("parameters->epsilon").value_or(0.02);
    auto B = Parameters::get<double>("parameters->Kb").value_or(0);
    double sigma = Parameters::get<double>("parameters->sigma").value_or(1.0);
    double c = Parameters::get<double>("parameters->c").value_or(1.0);
    auto phi = prob.solution(_phi);
    auto phi2 = prob.solution(_phi2);
    auto phi3 = prob.solution(_phi3);
    auto mu = prob.solution(_mu);
    auto mu2 = prob.solution(_mu2);
    auto mu3 = prob.solution(_mu3);
    auto kappaVec = prob.solution(_kappaVec);
    auto lambda1 = prob.solution(_lambda1);
    auto vGrid = prob.solution(_vGrid);

    DOFVector doubleWell1(prob.gridView(),lagrange(1));
    DOFVector doubleWell2(prob.gridView(),lagrange(1));
    DOFVector doubleWell3(prob.gridView(),lagrange(1));
    DOFVector phiMu1(prob.gridView(),lagrange(1));
    DOFVector phiMu2(prob.gridView(),lagrange(1));
    DOFVector phiMu3(prob.gridView(),lagrange(1));
    DOFVector oneNormGradPhi1(prob.gridView(),lagrange(1));
    DOFVector oneNormGradPhi2(prob.gridView(),lagrange(1));
    DOFVector oneNormGradPhi3(prob.gridView(),lagrange(1));
    DOFVector fBendMag(prob.gridView(),lagrange(1));
    DOFVector fBend(prob.gridView(),power<dow>(lagrange(1),flatInterleaved()));

    valueOf(doubleWell1) << 0.25/eps * pow<2>(phi)*pow<2>(1.0-phi);
    valueOf(doubleWell2) << 0.25/eps * pow<2>(phi2)*pow<2>(1.0-phi2);
    valueOf(doubleWell3) << 0.25/eps * pow<2>(phi3)*pow<2>(1.0-phi3);

    valueOf(phiMu1) << -phi*mu;
    valueOf(phiMu2) << -phi2*mu2;
    valueOf(phiMu3) << -phi3*mu3;

    valueOf(oneNormGradPhi1).interpolate(-0.5*eps* dot(gradientOf(phi),gradientOf(phi)),
                                         tag::average{});
    valueOf(oneNormGradPhi2).interpolate(-0.5*eps* dot(gradientOf(phi2),gradientOf(phi2)),
                                         tag::average{});
    valueOf(oneNormGradPhi3).interpolate(-0.5*eps* dot(gradientOf(phi3),gradientOf(phi3)),
                                         tag::average{});

    valueOf(fBend).interpolate(vGrid - c * gradientOf(lambda1)
                             - preFactor * valueOf(kappaVecOld) *
                             (valueOf(doubleWell1) + valueOf(doubleWell2) + valueOf(doubleWell3)
                             + valueOf(phiMu1) + valueOf(phiMu2) + valueOf(phiMu3)
                             + valueOf(oneNormGradPhi1) + valueOf(oneNormGradPhi2) + valueOf(oneNormGradPhi3)),
                                tag::average{});

    valueOf(fBendMag) << two_norm(valueOf(fBend));

    // store the pressure, velocity and laplace solution into the writer
    addWriterData(writer,valueOf(doubleWell1),"doubleWell1",1,
              valueOf(doubleWell2),"doubleWell2",1,
              valueOf(doubleWell3),"doubleWell3",1,
              valueOf(phiMu1),"phiTimesMu1",1,
              valueOf(phiMu2),"phiTimesMu2",1,
              valueOf(phiMu3),"phiTimesMu3",1,
              valueOf(oneNormGradPhi1),"normGradPhiSquared1",1,
              valueOf(oneNormGradPhi2),"normGradPhiSquared2",1,
              valueOf(oneNormGradPhi3),"normGradPhiSquared3",1,
              valueOf(fBendMag),"fBend",1,
              vGrid,"vGrid",2);


    timesteps.push_back(adaptInfo.time());
    auto path = Parameters::get<std::string>("output directory").value_or("output");

    writePVDFile(timesteps,"forceTerms",path,writer,1);
}



namespace AMDiS {
    template<class G, class T>
    auto myValueOf(ElementVector<G, T> const &ev) {
        return ElementGridFunction{ev.gridView(), ev.data()};
    }
}