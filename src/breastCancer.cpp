#include <amdis/AMDiS.hpp>
#include "helperFunctions.hpp"
#include "operatorFunctions.hpp"
#include <amdis/AdaptInstationary.hpp>
#include <amdis/ProblemInstat.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/GridFunctions.hpp>
#include <amdis/Marker.hpp>
#include <amdis/ElementVector.hpp>

#include <dune/curvedgrid/grid.hh>
#include <amdis/breastCancer/hostGridInterpolator.hpp>
#include <amdis/breastCancer/myIndices.hpp>
#include <amdis/breastCancer/writeFiles.hh>
#include <dune/grid/albertagrid.hh>


using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;
using namespace Dune::Indices;

using namespace MyIndices;
//the grid type to be used
using Grid = Dune::AlbertaGrid<1, 2>;

//the main method containing all your code
int main(int argc, char** argv)
{
    // this has to be called always in amdis
    Environment env(argc, argv);

    // using a file reader to read the mesh (the name must be given in the parameter file
    MeshCreator<Grid> meshCreator("chMesh");

    // the world dimension (in your case 2)
    const auto dow = Grid::dimensionworld;

    // store the mesh into a host grid
    auto gridPtr = meshCreator.create();

    // make a DOFVector filled with coordinates as GridFunction for the CurvedGrid
    GlobalBasis basis1(gridPtr->leafGridView(), power<dow>(lagrange<1>(),flatInterleaved()));
    auto coordinates = std::make_shared<DOFVector<decltype(basis1)>>(basis1);
    valueOf(*coordinates) << X();

    // the moving mesh...changing coordsDF updates the mesh!
    Dune::CurvedGrid grid{*gridPtr, valueOf(*coordinates)};

    // define the finite element basis for the coupled system
    // CAUTION: If you add or remove components from this list, change also list in myIndices.hpp and the
    // list of solution names below
    auto pbf = composite(lagrange<1>(),//phi
                         lagrange<1>(),//mu
                         lagrange<1>(),//phi2
                         lagrange<1>(),//mu2
                         lagrange<1>(),//phi3
                         lagrange<1>(),//mu3
                         power<dow>(lagrange<1>(),flatInterleaved()),//kappaVec
                         power<dow>(lagrange<1>(),flatInterleaved()),//vGrid
                         lagrange<1>(),//lambda1
                         lagrange<1>(),//diffusion concentration
                         flatLexicographic());

    // here, we create the framework for the problem
        ProblemStat prob("ch", grid, pbf);
        prob.initialize(INIT_ALL);

        ProblemInstat probInstat("ch", prob);
        probInstat.initialize(INIT_UH_OLD);

        AdaptInfo adaptInfo("adapt");

    // the (inverse of the) time step size
        auto invTau1 = 1.0/adaptInfo.timestep();
        auto tau1 = adaptInfo.timestep();
        auto invTau =  std::ref(invTau1);
        auto tau =  std::ref(tau1);

    // phi denotes the first solution of the system, mu the second one, xS the fourth and kappaVec the fifth...
        auto phi = prob.solution(_0);
        auto mu = prob.solution(_1);
        auto phi2 = prob.solution(_2);
        auto mu2 = prob.solution(_3);
        auto phi3 = prob.solution(_4);
        auto mu3 = prob.solution(_5);
        auto kappaVec = prob.solution(_6);
        auto vGrid = prob.solution(_7);
        auto lambda1 = prob.solution(_8);
        auto conc = prob.solution(_9);
        double E_sum = 0;

    // list of DOFVectors and ElementVectors needed later
        DOFVector displacement(prob.gridView(),power<dow>(lagrange(1),flatInterleaved()));
        DOFVector kappaVecOld(prob.gridView(),power<dow>(lagrange(1),flatInterleaved()));
        ElementVector distances(*prob.grid(),0.0);

    // here, all the terms of the PDE are added (in weak form)
        // 1st Cahn-Hilliard equation
            double source = Parameters::get<double>("parameters->q1").value_or(0.0);
            double source2 = Parameters::get<double>("parameters->q2").value_or(0.0);
            double source3 = Parameters::get<double>("parameters->q3").value_or(0.0);
            auto src = std::ref(source);
            auto src2 = std::ref(source2);
            auto src3 = std::ref(source3);

            double initialVal = Parameters::get<double>("source factor").value_or(0);
            double randomVal = Parameters::get<double>("source random").value_or(0);
            DOFVector randomDOF(prob.gridView(), lagrange(1));
            auto randomVals = [initialVal, randomVal](FieldVector<double, Grid::dimensionworld> const &x) {
                return (initialVal + randomVal * ((double) rand() / (RAND_MAX)));
            };
            valueOf(randomDOF) << randomVals;
            fillOperatorsCH(prob,_phi,_mu,valueOf(randomDOF),invTau,src,1);

            // 2nd Cahn-Hilliard equation
            fillOperatorsCH(prob,_phi2,_mu2,valueOf(randomDOF),invTau,src2,2);

            // 3rd Cahn-Hilliard equation
            double initialVal3 = Parameters::get<double>("source factor 3").value_or(0);
            double randomVal3 = Parameters::get<double>("source random 3").value_or(0);
            DOFVector randomDOF3(prob.gridView(), lagrange(1));
            auto randomVals3 = [initialVal3, randomVal3](FieldVector<double, Grid::dimensionworld> const &x) {
                return (initialVal3 + randomVal3 * ((double) rand() / (RAND_MAX)));
            };
            valueOf(randomDOF3) << randomVals3;

            fillOperatorsCH(prob,_phi3,_mu3,valueOf(randomDOF3),invTau,src3,3);

        // implementation of concentration diffusion equation
            fillOperatorsConc(prob, invTau);

        // computation of curvature vector
            fillOperatorsCurvature(prob,tau);

        // Bending Force
            fillOperatorsBending(prob, kappaVec);

        // surface tension like force
            fillOperatorsCHForce(prob,_phi,_mu);
            fillOperatorsCHForce(prob,_phi2,_mu2);
            fillOperatorsCHForce(prob,_phi3,_mu3);

        // grid velocity
            fillOperatorsVGrid(prob,distances,tau);


    // refinement information: how fine should the grid be at the interface and in the bulk? Given in parameter file as
    // a refinement level (0 - no refinement, n - bisection max. n times)
        int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
        int ref_max  = Parameters::get<int>("refinement->maximum kappaVec").value_or(10);
        int ref_bulk = Parameters::get<int>("refinement->bulk").value_or(2);

    // a marker which decides, whether an element should be refined or not, based on the value of phi
        GridFunctionMarker marker("interface", prob.grid(),
                                  invokeAtQP([ref_int, ref_bulk](double phi, double phi2, double phi3) {
                                      return (phi > 0.05 && phi < 0.95)
                                      || (phi2 > 0.05 && phi2 < 0.95)
                                      || (phi3 > 0.05 && phi3 < 0.95) ? ref_int : ref_bulk;
                                  }, phi, phi2, phi3));
        prob.addMarker(marker);

    // a second marker which decides, whether an element should be refined or not, based on the value of kappaVec
        GridFunctionMarker marker2("interface2", prob.grid(),
                                  invokeAtQP([ref_max,ref_bulk](FieldVector<double,dow> kappaVec) {
                                      return  std::min(ref_max,std::max((int)(kappaVec.two_norm()/5),ref_bulk));
                                  }, valueOf(kappaVec)));
        prob.addMarker(marker2);

    // here, an initial phase field on the grid is created, based on a 2D circle or random values
        computeInitialPhaseField(prob,phi,adaptInfo,1);
        computeInitialPhaseField(prob,phi2,adaptInfo,2);
        computeInitialPhaseField(prob,phi3,adaptInfo,3);

    // compute initial grid shape with a function
    auto prescribe = Parameters::get<int>("prescribe initial shape with function").value_or(0);
    if (prescribe) {
        auto amplitude = Parameters::get<double>("shape function amplitude").value_or(0);
        auto period = Parameters::get<double>("shape function period").value_or(0);

        valueOf(*coordinates) << [period, amplitude](FieldVector<double, Grid::dimensionworld> const &x) {
            double alpha = std::abs(x[0]) > 1.e-8 ? atan(x[1] / x[0]) : x[1] > 0 ? M_PI / 2.0 : -M_PI / 2.0;
            return (x + amplitude * cos(period * alpha) * x / x.two_norm());
        };
    }

    // prepare for writing extra output files
    std::vector<double> timesteps;
    Dune::VTKWriter writer(prob.gridView(),Dune::VTK::conforming);

    // here, the problem is solved
    AdaptInstationary adapt("adapt", prob, adaptInfo, probInstat, adaptInfo);
    {
        AMDIS_FUNCNAME("AdaptInstationary::adapt()");
        int errorCode = 0;

        test_exit(adaptInfo.timestep() >= adaptInfo.minTimestep(),
                  "timestep < min timestep");
        test_exit(adaptInfo.timestep() <= adaptInfo.maxTimestep(),
                  "timestep > max timestep");

        test_exit(adaptInfo.timestep() > 0, "timestep <= 0!");

        // stuff to be done in the first time step
        if (adaptInfo.timestepNumber() == 0)
        {
            adaptInfo.setTime(adaptInfo.startTime());
            adapt.initialAdaptInfo().setStartTime(adaptInfo.startTime());
            adapt.initialAdaptInfo().setTime(adaptInfo.startTime());

            adapt.problemTime()->setTime(adaptInfo);

            // initial adaption
            adapt.problemTime()->solveInitialProblem(adapt.initialAdaptInfo());
            adapt.problemTime()->transferInitialSolution(adaptInfo);
            writeForceContributions(writer,prob, kappaVecOld, timesteps,adaptInfo);
        }

        // the time step loop
        bool breakWhenStable = false;
        Parameters::get("adapt->break when stable", breakWhenStable);
        while (!adaptInfo.reachedEndTime())
        {
            //unset source after n time steps
            auto num = Parameters::get<int>("number of time steps for q1").value_or(10);
            if (adaptInfo.timestepNumber() == num) source = 0.0;

            auto num2 = Parameters::get<int>("number of time steps for q2").value_or(10);
            if (adaptInfo.timestepNumber() == num2) source2 = 0.0;

            auto num3 = Parameters::get<int>("number of time steps for q3").value_or(10);
            if (adaptInfo.timestepNumber() == num3) source3 = 0.0;

            adapt.problemTime()->initTimestep(adaptInfo);
            adaptInfo.setTimestepIteration(0);

            // estimate before first adaption
            if (adaptInfo.time() <= adaptInfo.startTime())
                adapt.problemIteration()->oneIteration(adaptInfo, ESTIMATE);


            // increment time
            adaptInfo.setTime(adaptInfo.time() + adaptInfo.timestep());

            adapt.problemTime()->setTime(adaptInfo);

            msg("time = {}, timestep = {}", adaptInfo.time(), adaptInfo.timestep());

            adaptInfo.setSpaceIteration(0);

            // do the iteration
            adapt.problemIteration()->beginIteration(adaptInfo);
            adapt.problemIteration()->oneIteration(adaptInfo, FULL_ITERATION);
            adapt.problemIteration()->endIteration(adaptInfo);
            adaptInfo.setLastProcessedTimestep(adaptInfo.timestep());

            adaptInfo.incTimestepNumber();
            adapt.problemTime()->closeTimestep(adaptInfo);

            // compute and write post processing output data
            int every = Parameters::get<int>("every").value_or(1);
            if (adaptInfo.timestepNumber()%every == 0) {
                writePostProcessingData(prob, adaptInfo, E_sum);
                writer.clear();
                writeForceContributions(writer, prob, kappaVecOld, timesteps, adaptInfo);
            }
            //computation of grid movement displacement
            valueOf(displacement) << tau * valueOf(vGrid);

            // actual grid update: the values of coords are set so the coodinates DOFVector
            DOFVector coords(prob.gridView(),power<dow>(lagrange(1),flatInterleaved()));
            valueOf(coords) << X() + valueOf(displacement);
            interpolateHostGrid(valueOf(*coordinates),valueOf(coords));

            // compute accurate curvature vector of last time step
            computeKappaVecOld(kappaVecOld,prob.grid());
            kappaVec << valueOf(kappaVecOld);

            // compute element-wise stretches
            for (auto const& e : elements(prob.gridView()))
                distances.data()[prob.gridView().indexSet().index(e)] = e.geometry().volume();

            for (auto const& e : elements(prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView()))
                distances.data()[prob.grid()->hostGrid()->hostGrid().hostGrid()->leafGridView().indexSet().index(e)] /= e.geometry().volume();

            if (breakWhenStable && (adaptInfo.solverIterations() == 0))
                break;
        }
        return errorCode;
    }

    return 0;
}
