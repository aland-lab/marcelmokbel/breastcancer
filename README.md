
Automatic installation
=========================

Download the file `installAMDiS.sh`, copy it into your project folder. Check your clang version and location 
(typically something like `/usr/bin/clang++14` if you have clang 14). Compare
to `lines 105 and 106` of `installAMDiS.sh` and change paths to your clang version. 

Then run
```
sh installAMDiS.sh
```
This will automatically install all dune and AMDiS modules and it will also download and install the breastCancer
module to the folder `breastcancer`.  
There, you can just type
```
build-clang/src/breastCancer init/breastCancer.2d
```

to run your code. And if you want to re-compile your code, just type (replacing `path/to/dune` and `/path/to/amdis` with your local 
directories)
```
export DUNE_CONTROL_PATH=/path/to/amdis:/path/to/dune  
/path/to/dune/dune-common/bin/dunecontrol --current all 
```
Preparing the Sources
=========================

Information about AMDiS can be found on  
`https://amdis.readthedocs.io/en/latest/`

Additional to dune and AMDiS, you'll need the
following programs installed on your system:

cmake >= 3.1, Alberta, metis, openmpi, parmetis, suitesparse. In Linux, run the following, to get them

````
sudo apt-get install \
libalberta-dev \
libmetis-dev \
libopenmpi-dev \
libparmetis-dev \
libsuitesparse-dev
````  
You'll also need the following dune modules:
amdis,
dune-common,
dune-istl,
dune-geometry, 
dune-uggrid, 
dune-grid, 
dune-localfunctions, 
dune-typetree, 
dune-functions, 
dune-alugrid, 
dune-foamgrid, 
dune-vtk, 
dune-curvedgeometry, 
dune-curvedgrid, 
amdis-extensions

Therefore, do the following:  
````
mkdir DUNE_DIR  
cd DUNE_DIR 
git clone https://gitlab.com/amdis/amdis.git
git clone https://gitlab.dune-project.org/core/dune-common.git  
git clone https://gitlab.dune-project.org/core/dune-istl.git  
git clone https://gitlab.dune-project.org/core/dune-geometry.git  
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git  
git clone https://gitlab.dune-project.org/core/dune-grid.git  
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git  
git clone https://gitlab.dune-project.org/staging/dune-typetree.git    
git clone https://gitlab.dune-project.org/staging/dune-functions.git  
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git  
git clone https://gitlab.dune-project.org/extensions/dune-foamgrid.git  
git clone https://gitlab.dune-project.org/extensions/dune-vtk.git   
git clone https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgeometry.git  
git clone https://gitlab.mn.tu-dresden.de/iwr/dune-curvedgrid.git  
git clone https://gitlab.mn.tu-dresden.de/amdis/amdis-extensions.git  
dune-common/bin/dunecontrol all
````

Getting started
---------------

If these preliminaries are met, you should run  
```
cd /path/to/breastcancer  
export DUNE_CONTROL_PATH=DUNE_DIR/amdis:DUNE_DIR 
DUNE_DIR/dune-common/bin/dunecontrol --current all  
```

which will find all installed dune modules as well as all dune modules
(not installed) which sources reside in a subdirectory of the current
directory. 

To run your code, you can now execute
```
build-cmake/src/breastCancer init/breastCancer.2d
```

Note that if dune is not installed properly you will either
have to add the directory where the dunecontrol script resides (probably
./dune-common/bin) to your path or specify the relative path of the script.

Most probably you'll have to provide additional information to dunecontrol
(e.g. compilers, configure options) and/or make options.

The most convenient way is to use options files in this case. The files
define four variables:

CMAKE_FLAGS      flags passed to cmake (during configure)

An example options file might look like this:

#use this options to configure and make if no other options are given
```
CMAKE_FLAGS="-DCMAKE_CXX_COMPILER=/usr/bin/clang++-14 
-DCMAKE_C_COMPILER=/usr/bin/clang-14
-DDUNE_ENABLE_PYTHONBINDINGS:BOOL=0"
MAKE_FLAGS="-j2"
BUILDDIR="build-clang"
```
If you save this information into example.opts you can pass the opts file to
dunecontrol via the --opts option, e.g.
```
dunecontrol --opts=example.opts all
```
More info
---------

See

     dunecontrol --help

for further options.


The full build system is described in the dune-common/doc/buildsystem (Git version) or under share/doc/dune-common/buildsystem if you installed DUNE!
