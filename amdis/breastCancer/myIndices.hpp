#ifndef BREASTCANCER_MYINDICES_H
#define BREASTCANCER_MYINDICES_H

namespace MyIndices {
    static constexpr auto _phi = Dune::Indices::_0;
    static constexpr auto _mu = Dune::Indices::_1;
    static constexpr auto _phi2 = Dune::Indices::_2;
    static constexpr auto _mu2 = Dune::Indices::_3;
    static constexpr auto _phi3 = Dune::Indices::_4;
    static constexpr auto _mu3 = Dune::Indices::_5;
    static constexpr auto _kappaVec = Dune::Indices::_6;
    static constexpr auto _vGrid = Dune::Indices::_7;
    static constexpr auto _lambda1 = Dune::Indices::_8;
    static constexpr auto _conc = Dune::Indices::_9;
}

#endif //BREASTCANCER_MYINDICES_H
