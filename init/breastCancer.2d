dimension of world:   2

% /path/to/meshFile
chMesh->macro file name: ./macro/circle.msh     % use ./macro/circle.msh for a circle
chMesh->global refinements: 0

% initial shape of the mesh prescribed with a given cos function
prescribe initial shape with function: 1
shape function amplitude: 0.06
shape function period: 16

% mesh refinement information
refinement->interface: 2                        % maximum refinement level at the interface
refinement->maximum kappaVec: 2                 % maximum refinement level depending on the curvature
refinement->bulk: 2                             % refinement level in the bulk


% =============== phase field parameters =========================================================

%%% initial values

% should initial phi_1 be made with random values, or with the ellipse/circle from the above parameters?
circles or initial value: 1                     % 1 if random, 0 if ellipse/circle, following 4 parameters only used, if this is 1
    phase field initial value: 1.0              % phi_1 = value + random number
    phase field initial value random: 0.0
    initial radius: 2                           % phi_1 nonzero only within the disk between radius inside and radius
    initial radius inside: 0.5

% should initial phi_2 be made with random values, or with the ellipse/circle from the above parameters?
circles or initial value 2: 1                   % 1 if random, 0 if ellipse/circle, following 4 parameters only used, if this is 1
    phase field initial value 2: 1.0            % phi_2 = value + random number
    phase field initial value random 2: 0.0
    initial radius 2: 2                         % phi_2 nonzero only within the disk between radius inside and radius
    initial radius inside 2: 0

% should initial phi_3 be made with random values, or with the ellipse/circle from the above parameters?
circles or initial value 3: 1                   % 1 if random, 0 if ellipse/circle, following 4 parameters only used, if this is 1
    phase field initial value 3: 1.0            % phi_2 = value + random number
    phase field initial value random 3: 0.0
    initial radius 3: 2                         % phi_2 nonzero only within the disk between radius inside and radius
    initial radius inside 3: 0


%%% if no random values, choose an ellipse, with phi=1 inside and phi=0 outside the ellipse.

%for first phase field: phi_1=1 within this ellipse, phi_1=0 else
parameters->radius1: 10.75
parameters->radius2: 10.75
parameters->center: 1.0 0.0

%for second phase field: phi_2=1 within this ellipse, phi_2=0 else
parameters->radius1_2: 10.5
parameters->radius2_2: 10.5
parameters->center_2: 1.0 0.0

%for third phase field: phi_3=1 within this ellipse, phi_3=0 else
parameters->radius1_3: 10.5
parameters->radius2_3: 10.5
parameters->center_3: 1.0 0.0

%%% model parameters

parameters->epsilon: 0.03                       % interface width for both phase fields
parameters->M: 20                               % Cahn-Hilliard mobility for both phase fields

%%% proliferation of the first species
parameters->q1: 15                              % source term: q1 * (source factor*random number) with random number = source random
    source factor: 1.0
    source random: 0.0
number of time steps for q1: 350                % after this number of time steps, the source term is set to zero

%%% proliferation of the second species
parameters->q2: 5                               % source term q2
threshold: 0.75                                 % only growth below this threshold
number of time steps for q2: 700                % after this number of time steps, the source term is set to zero

%%% proliferation of the third species
parameters->q3: 0                              % source term: q1 * (source factor*random number) with random number = source random
    source factor 3: 1.0
    source random 3: 0.0
number of time steps for q3: 350                % after this number of time steps, the source term is set to zero


% =============== shell force parameters ==================================================

parameters->k: 1                                % factor for shell force
parameters->Kb: 0.01                            % bending stiffness
parameters->c: 10000                            % tangential force to keep point distances
parameters->sigma: 1                            % Cahn-Hilliard surface tension
parameters->diffusion: 1                        % the diffusion of the concentration equation

% time information for your simulations:
adapt->timestep:    1.e-4
adapt->start time:  0.0
adapt->end time:    0.2

% ============================ remaining parameters for solver and output =============================================

output directory: output
post processing output filename: ppData

every: 10

ch->mesh:                       chMesh
ch->solver:                     direct
ch->solver->max iteration:      1000
ch->solver->relative tolerance: 1e-6
ch->solver->info: -1

ch->output[0]->format:             vtk
ch->output[0]->filename:           phi.2d
ch->output[0]->subsampling:        1
ch->output[0]->output directory:   ${output directory}
ch->output[0]->name:               phi
ch->output[0]->mode:               1
ch->output[0]->animation:          1
ch->output[0]->write every i-th timestep:	$every

ch->output[1]->format:             vtk
ch->output[1]->filename:           mu.2d
ch->output[1]->output directory:   ${output directory}
ch->output[1]->name:               mu
ch->output[1]->mode:               1
ch->output[1]->animation:          1
ch->output[1]->write every i-th timestep:	$every

ch->output[2]->format:             vtk
ch->output[2]->filename:           phi2.2d
ch->output[2]->subsampling:        1
ch->output[2]->output directory:   ${output directory}
ch->output[2]->name:               phi2
ch->output[2]->mode:               1
ch->output[2]->animation:          1
ch->output[2]->write every i-th timestep:	$every

ch->output[3]->format:             vtk
ch->output[3]->filename:           mu2.2d
ch->output[3]->output directory:   ${output directory}
ch->output[3]->name:               mu2
ch->output[3]->mode:               1
ch->output[3]->animation:          1
ch->output[3]->write every i-th timestep:	$every

ch->output[4]->format:             vtk
ch->output[4]->filename:           phi3.2d
ch->output[4]->subsampling:        1
ch->output[4]->output directory:   ${output directory}
ch->output[4]->name:               phi3
ch->output[4]->mode:               1
ch->output[4]->animation:          1
ch->output[4]->write every i-th timestep:	$every

ch->output[5]->format:             vtk
ch->output[5]->filename:           mu3.2d
ch->output[5]->output directory:   ${output directory}
ch->output[5]->name:               mu3
ch->output[5]->mode:               1
ch->output[5]->animation:          1
ch->output[5]->write every i-th timestep:	$every


ch->output[6]->format:             vtk
ch->output[6]->filename:           kappaVec.2d
ch->output[6]->output directory:   ${output directory}
ch->output[6]->name:               kappaVec
ch->output[6]->mode:               1
ch->output[6]->animation:          1
ch->output[6]->write every i-th timestep:	$every

ch->output[7]->format:             vtk
ch->output[7]->filename:           vGrid.2d
ch->output[7]->output directory:   ${output directory}
ch->output[7]->name:               vGrid
ch->output[7]->mode:               1
ch->output[7]->animation:          1
ch->output[7]->write every i-th timestep:	$every

ch->output[9]->format:             vtk
ch->output[9]->filename:           c.2d
ch->output[9]->output directory:   ${output directory}
ch->output[9]->name:               c
ch->output[9]->mode:               1
ch->output[9]->animation:          1
ch->output[9]->write every i-th timestep:	$every



kappaVecOld->solver: direct
kappaVecOld->solver->relative tolerance: 1e-8
kappaVecOld->solver->info:	-1